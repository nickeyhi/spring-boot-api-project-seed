package com.douyou.qr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DouyouQrApplication {
    public static void main(String[] args) {
        SpringApplication.run(DouyouQrApplication.class, args);
    }
}

