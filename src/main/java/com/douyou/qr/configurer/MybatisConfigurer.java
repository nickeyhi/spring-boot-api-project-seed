package com.zycfc.sys.template.configurer;

import com.github.pagehelper.PageInterceptor;
import com.zycfc.sys.template.core.ProjectConstant;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import tk.mybatis.spring.mapper.MapperScannerConfigurer;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Mybatis & Mapper & PageHelper 配置
 */
@Configuration
public class MybatisConfigurer {
    @Bean
    public SqlSessionFactory sqlSessionFactoryBean(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
        factory.setDataSource(dataSource);
        //factory.setTypeAliasesPackage(ProjectConstant.MODEL_PACKAGE);//去除这些包的别名
        org.apache.ibatis.session.Configuration configuration =new org.apache.ibatis.session.Configuration();
        //驼峰转换开启
        configuration.setMapUnderscoreToCamelCase(true);
        //oracle 实体对象null转换为数据库null
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        factory.setConfiguration(configuration);

        //配置分页插件，详情请查阅官方文档
        Properties properties = new Properties();
        //分页尺寸为0时查询所有纪录不再执行分页
        properties.setProperty("pageSizeZero", "flase");
        //页码<=0 查询第一页，页码>=总页数查询最后一页
        properties.setProperty("reasonable", "false");
        //支持通过 Mapper 接口参数来传递分页参数
        properties.setProperty("supportMethodsArguments", "true");

        PageInterceptor pageInterceptor = new PageInterceptor();
        pageInterceptor.setProperties(properties);
        //添加插件
        factory.setPlugins(new Interceptor[]{pageInterceptor});

        //添加XML目录
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        factory.setMapperLocations(resolver.getResources("classpath:mapper/*.xml"));
        return factory.getObject();
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactoryBean");
        mapperScannerConfigurer.setBasePackage(ProjectConstant.MAPPER_PACKAGE);

        //配置通用Mapper，详情请查阅官方文档
        Properties properties = new Properties();
        properties.setProperty("mappers", ProjectConstant.MAPPER_INTERFACE_REFERENCE);
        //insert、update是否判断字符串类型!='' 即 test="str != null"表达式内是否追加 and str != ''
        properties.setProperty("notEmpty", "false");
        //properties.setProperty("IDENTITY", "MYSQL");可以指定数据库类型，也可以不指定，会自动获取
        //properties.setProperty("ORDER","BEFORE"); 在oracle 中用 @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "select SEQ_SYS_DICT.nextval from dual")生成主键时需要
        mapperScannerConfigurer.setProperties(properties);
        return mapperScannerConfigurer;
    }

}

